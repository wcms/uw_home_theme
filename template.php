<?php

/**
 * @file
 * Template.php.
 */

/**
 * Implements hook_page_alter().
 */
function uw_home_theme_page_alter(&$page) {
  // Force the global footer region to display when empty.
  if (!isset($page['global_footer'])) {
    foreach (system_region_list($GLOBALS['theme'], REGIONS_ALL) as $region => $name) {
      if (in_array($region, array('global_footer'))) {
        $page['global_footer'] = array(
          '#region' => 'global_footer',
          '#weight' => '-10',
          '#theme_wrappers' => array('region'),
        );
      }
    }
  }
}
/**
 * Implements template_preprocess_block().
 */
function uw_home_theme_preprocess_block(&$variables) {
  // Add @aria-label to responsive_menu_combined block.
  if ($variables['block']->bid === 'responsive_menu_combined-responsive_menu_combined') {
    $variables['attributes_array']['aria-label'] = 'Main';
  }
}

/**
 * Implementation of theme_qt_quicktabs_tabset().
 *
 * Quicktabs does not properly add @id elements to link to
 * (when javascript is disabled),
 * so we change the tab fragment option to match.
 */
function uw_home_theme_qt_quicktabs_tabset($vars) {
  $variables = array(
    'attributes' => array(
      'class' => 'quicktabs-tabs quicktabs-style-' . $vars['tabset']['#options']['style'],
    ),
    'items' => array(),
  );
  foreach (element_children($vars['tabset']['tablinks']) as $key) {
    $item = array();
    if (is_array($vars['tabset']['tablinks'][$key])) {
      $tab = $vars['tabset']['tablinks'][$key];
      if ($key == $vars['tabset']['#options']['active']) {
        $item['class'] = array('active');
      }

      // Make sure the tab fragment (part following the #hashtag)
      // points to the id of the tab page, only replace the first instance.
      $replace_count = 1;
      $tab['#options']['fragment'] = str_replace('qt-', 'quicktabs-tabpage-', $tab['#options']['fragment'], $replace_count) . '-' . $key;

      $item['data'] = drupal_render($tab);
      $variables['items'][] = $item;
    }
  }
  return theme('item_list', $variables);
}

/**
 * Implementation of template_preprocess_html.
 *
 * Add an "org_default" class to the body.
 * Add an "emergency" class to the body if we
 * have a major alert (emergency darksite mode).
 */
function uw_home_theme_preprocess_html(&$variables) {
  $variables['classes_array'][] = 'org_default';
  if (!empty($variables['page']['alert-major'])) {
    $variables['classes_array'][] = 'emergency';
  }
  // Add meta tags to enable mobile view.
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    ),
  );
  $meta_mobile_view = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1',
    ),
  );
  // Add meta tag for Facebook domain verification.
  $meta_facebook_domain_verification = array (
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'facebook-domain-verification',
      'content' => 'ujau49j2mxdvujb0kle7m8xgm73pc7',
    ),
  );
  $rwd_home_path = drupal_get_path('theme', 'uw_home_theme') . '/css/rwd-home.css';
  drupal_add_css($rwd_home_path);
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_mobile_view, 'meta_mobile_view');
  drupal_add_html_head($meta_facebook_domain_verification, 'meta_facebook_domain_verification');
}

/**
 * Implements hook_css_alter().
 */
function uw_home_theme_css_alter(&$css) {
  // uw_core_theme and uw_fdsu_theme are loaded, but we don't want their CSS.
  // Remove any CSS added by these.
  $path = drupal_get_path('theme', 'uw_core_theme') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($path)) == $path) {
      unset($css[$key]);
    }
  }
  $path_fdsu = drupal_get_path('theme', 'uw_fdsu_theme') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($path_fdsu)) == $path_fdsu) {
      unset($css[$key]);
    }
  }

  // Remove CSS added by various modules.
  $path_homepage_directories = drupal_get_path('module', 'homepage_directories') . '/css/';
  $path_global_header = drupal_get_path('module', 'uw_nav_global_header') . '/css/';
  $path_cta = drupal_get_path('module', 'uw_ct_embedded_call_to_action') . '/css/';
  $path_ctools = drupal_get_path('module', 'ctools') . '/css/';
  $path_global_footer = drupal_get_path('module', 'uw_nav_global_footer') . '/css/';
  $path_site_footer = drupal_get_path('module', 'uw_nav_site_footer') . '/css/';
  $path_site_share = drupal_get_path('module', 'uw_social_media_sharing') . '/css/';
  $path_quicktabs = drupal_get_path('module', 'quicktabs') . '/css/';
  $path_system_theme = drupal_get_path('module', 'system') . '/';
  $path_ff = drupal_get_path('module', 'uw_ct_embedded_facts_and_figures') . '/css/';
  $path_ig = drupal_get_path('module', 'uw_ct_image_gallery') . '/css/';
  $exclude = array(
    $path_cta . 'uw_ct_embedded_call_to_action.css' => TRUE,
    $path_homepage_directories . 'homepage-directories.css' => TRUE,
    $path_ctools . 'modal.css' => TRUE,
    $path_global_header . 'uw_nav_global_header.css' => TRUE,
    $path_quicktabs . 'quicktabs.css' => TRUE,
    $path_global_footer . 'uw_nav_global_footer.css' => TRUE,
    $path_site_footer . 'uw_nav_site_footer.css' => TRUE,
    $path_site_share . 'uw_social_media_sharing.css' => TRUE,
    $path_system_theme . 'system.base.css' => TRUE,
    $path_system_theme . 'system.theme.css' => TRUE,
    $path_ff . 'highlighted_fact.css' => TRUE,
    $path_ig . 'image-galleries.css' => TRUE,
    $path_ig . 'uw_ct_image_gallery.css' => TRUE,

  );
  $css = array_diff_key($css, $exclude);
}

/**
 * Implements hook_js_alter().
 */
function uw_home_theme_js_alter(&$javascript) {
  global $base_path;
  global $base_url;
  if (module_exists('uw_header_search')) {
    $rwdjs_path = drupal_get_path('module', 'uw_header_search') . '/uw_header_search.js';
    drupal_add_js($rwdjs_path);
    // drupal_add_js($tabsjs_path);
    drupal_add_js(array(
      'CToolsModal' => array(
        'modalSize' => array(
          'type' => 'scale',
          'width' => 1,
          'height' => 1,
        ),
        'modalOptions' => array(
          'opacity' => .98,
          'background-color' => '#252525',
        ),
        'animation' => 'fadeIn',
        'animationSpeed' => 'slow',
        'throbberTheme' => 'CToolsThrobber',
        // Customize the AJAX throbber like so:
        // This function assumes the images are inside
        // the module directory's "images" directory:
        'throbber' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t('Loading...'),
          'title' => t('Loading'),
        )),
        'closeText' => '',
        'closeImage' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t(''),
          'title' => t(''),
        )),
      ),
    ), 'setting');
    $exclude = array();
    $javascript = array_diff_key($javascript, $exclude);
  }
}
