<?php

/**
 * @file
 */
?>
<div class="breakpoint"></div>
<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site"<?php print $attributes; ?>>
    <div class="uw-site--inner">
        <div class="uw-section--inner">
            <div id="skip" class="skip">
                <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
                <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
            </div>
        </div>
        <div id="header" class="uw-header--global">
            <?php
            $global_message = file_get_contents('https://uwaterloo.ca/global-message.html');
            if ($global_message) {
                ?>
                <div id="global-message">
                    <?php echo $global_message; ?>
                </div>
                <?php
            }
            ?>
            <div class="uw-section--inner">
                <?php print render($page['global_header']); ?>
            </div>
        </div>
        <div id="site--offcanvas" class="uw-site--off-canvas">
            <div class="uw-section--inner">
                <?php print render($page['site_header']); ?>
            </div>
        </div>
        <div id="site-colors" class="uw-site--colors">
            <div class="uw-section--inner">
                <div class="uw-site--cbar">
                    <div class="uw-site--c1 uw-cbar"></div>
                    <div class="uw-site--c2 uw-cbar"></div>
                    <div class="uw-site--c3 uw-cbar"></div>
                    <div class="uw-site--c4 uw-cbar"></div>
                </div>
            </div>
        </div>
        <div id="site-header" class="uw-site--header">
            <div class="uw-section--inner">
                <h1><?php print $title ? $title : $site_name; ?></h1>
                <!-- pathway -->
                <?php if (!empty($page['pathway-links'])): ?>
                    <!-- pathway menu links, only on pathway pages -->
                    <div id="pathway-links">
                        <?php print render($page['pathway-links']); ?>
                    </div>
                    <!-- /pathway links -->
                <?php endif; ?>
            </div>
        </div>
        <div class="uw-site--alert">
            <div class="uw-section--inner">
                <?php if (!empty($page['alert-major'])): ?>
                    <!-- alert-major -->
                    <div id="emergency">
                        <?php print render($page['alert-major']); ?>
                    </div>
                    <!-- /#alert-major -->
                <?php endif; ?>
                <?php if (!empty($page['alert'])): ?>
                    <!-- alert -->
                    <div id="alert">
                        <?php print render($page['alert']); ?>
                    </div>
                    <!-- /#alert -->
                <?php endif; ?>
            </div>
        </div>
        <div class="uw-site--banner">
            <div class="uw-section--inner">
                <div id="featured" class="featured">
                    <?php print render($page['banner']); ?>
                </div>
            </div><!-- /uw-headeranner__alt -->
        </div>
        <?php if (!empty($page['top_links'])): ?>
            <div class="uw-site--toplinks">
                <div class="uw-section--inner">
                    <!-- toplinks for pathway pages -->
                    <div class="toplinks">
                        <?php print render($page['top_links']); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <!-- /#toplinks section -->
        <div class="uw-site--notifications">
            <div class="uw-section--inner">
                <div class="uw-site--messages">
                    <?php print $messages; ?>
                </div>
                <div class="uw-site--help">
                    <?php print render($page['help']); ?>
                </div>
                <?php if ($tabs): ?>
                    <div class="node-tabs uw-site-admin--tabs">
                        <?php print render($tabs); ?>
                    </div>
                <?php endif; ?>
                <?php if ($action_links): ?>
                    <ul class="action-links">
                        <?php print render($action_links); ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
        <!-- /#notification section -->
        <div class="uw-site-main--content" id="main" role="main">
            <div class="uw-section--inner">
                <div id="content" class="uw-site-content">
                    <?php if (!empty($page['content'])): ?>
                        <?php print render($page['content']); ?>
                    <?php endif; ?>
                </div>
                <div id="site-sidebar" class="uw-site-sidebar">
                    <div class="uw-site-sidebar--second">
                        <?php print render($page['sidebar_second']); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#main site content section -->
    </div>
    <!-- ./uw-site inner-->
    <div class="uw-site--watermark">
        <div class="uw-section--inner"></div>
    </div>
    <div id="footer" class="uw-footer" role="contentinfo">
        <div id="uw-site-share" class="uw-site-share">
            <div class="uw-section--inner">
                <div class="uw-section-share"></div>
                <ul class="uw-site-share-top">
                    <li class="uw-site-share--button__top">
                        <a href="#" id="uw-top-button" class="uw-top-button">
                            <i class="ifdsu fdsu-arrow"></i>
                            <span class="uw-footer-top-word">TOP</span>
                        </a>
                    </li>
                    <li class="uw-site-share--button__share">
                        <a href="#" class="uw-footer-social-button">
                            <i class="ifdsu fdsu-share"></i>
                            <span class="uw-footer-share-word">Share</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <?php print render($page['global_footer']); ?>
    </div><!--/footer-->
</div><!--/site-->