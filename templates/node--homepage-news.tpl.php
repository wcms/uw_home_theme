<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Article" it would result in "node-article". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - view-mode-[view_mode]: The View Mode of the node e.g. teaser or full.
 *   - preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - promoted: Nodes promoted to the front page.
 *   - sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node entity. Contains data that may not be safe.
 * - $type: Node type, i.e. page, article, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

<?php
  // If there is a date in the news, then use it, if not used node create date.
  if(isset($content['field_homepage_news_date']['#items'][0]['value'])) {
    $date = strtotime($content['field_homepage_news_date']['#items'][0]['value']);
  }
  else {
    $date = $node->created;
  }

  // Seperate date into month day, year.
  $day = date('j', $date);
  $month = date('M', $date);
  $year = date('Y', $date);

  // Add this class if an image is present -- for adjusting margins on the title and teaser.
  $image_class = isset($content['field_related_image']) ? 'image' : '';
  // Add this class if the news item is sticked.
  $sticky_class = $sticky ? 'sticky' : '';
?>

<a class="news-item <?php print $image_class ?> <?php print $sticky_class ?> clearfix" href="<?php print render($content['field_link_url']['#items'][0]['display_url']); ?>">
  <span class="uw-news-date"><?php print $month . ' ' . $day . ', ' . $year; ?></span>
  <span class="uw-news-title"><?php print $title; ?></span>
  <span class="uw-news-teaser"><?php print render($content['body']); ?></span>
</a>
<div class="uw-news-tags">
  <?php if(isset($content['field_homepage_news_taxonomy'][0])): ?>
    <span class="uw-news-tags-item">
      <?php
        $counter = 0;
        while(isset($content['field_homepage_news_taxonomy'][$counter])) {
          if($counter == 0) {
            print $content['field_homepage_news_taxonomy'][$counter]['#markup'];
          }
          else {
            print ', ' . $content['field_homepage_news_taxonomy'][$counter]['#markup'];
          }
          $counter++;
        }
      ?>
    </span>
  <?php endif; ?>
</div>
