<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Article" it would result in "node-article". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - view-mode-[view_mode]: The View Mode of the node e.g. teaser or full.
 *   - preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - promoted: Nodes promoted to the front page.
 *   - sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node entity. Contains data that may not be safe.
 * - $type: Node type, i.e. page, article, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<?php
  $faculty = '';
  if(isset($content['field_faculty_author']['#items'][0]['value'])) {
    switch($content['field_faculty_author']['#items'][0]['value']) {
      case 'org_default_bg':
        $faculty = '';
        break;

      case 'org_ahs_bg':
        $faculty = 'Faculty of Health';
        break;

      case 'org_art_bg':
        $faculty = 'Faculty of Arts';
        break;

      case 'org_eng_bg':
        $faculty = 'Faculty of Engineering';
        break;

      case 'org_env_bg':
        $faculty = 'Faculty of Environment';
        break;

      case 'org_mat_bg':
        $faculty = 'Faculty of Mathematics';
        break;

      case 'org_sci_bg':
        $faculty = 'Faculty of Science';
        break;

      case 'org_cgc_bg':
        $faculty = 'Conrad Grebel University College';
        break;

      case 'org_ren_bg':
        $faculty = 'Renison University College';
        break;

      case 'org_stj_bg':
        $faculty = "St. Jerome's University";
        break;

      case 'org_stp_bg':
        $faculty = "St. Paul's University College";
        break;

      default:
        $faculty = '';
        break;
    }
  }
?>
<?php
  global $base_path;
  $theme_path = $base_path . drupal_get_path('theme', 'uw_home_theme');
  $banner_alt = $field_related_image[0]['alt'];
  $banner_small = image_style_url('homepage_feature_stories', $field_related_image[0]['uri']);
  $banner_square = image_style_url('homepage_feature_stories_small', $field_related_image[0]['uri']);
  $banner_med = image_style_url('homepage_feature_stories_medium', $field_related_image[0]['uri']);
  $banner_large = image_style_url('homepage_feature_stories_large', $field_related_image[0]['uri']);
  $banner_xl = image_style_url('homepage_feature_stories_xl', $field_related_image[0]['uri']);
?>
<div class="banner-item <?php print render($content['field_faculty_author']); ?>">
  <a href="<?php print render($content['field_link_url']['#items'][0]['display_url']); ?>">
    <div class="banner-image">
      <picture>
       <source srcset="<?php print $banner_xl;?>" media="(min-width: 1280px)">
        <source srcset="<?php print $banner_med;?>" media="(min-width: 1024px)">
        <source srcset="<?php print $banner_large;?>" media="(min-width: 769px)">
        <source srcset="<?php print $banner_square;?>" media="(min-width: 480px)">
        <source srcset="<?php print $banner_large;?>" media="(min-width: 320px)">
        <source srcset="<?php print $banner_large;?>">
        <img src="<?php echo $banner_xl; ?>" alt="<?php echo $banner_alt; ?>">
      </picture>
    </div>
   <div class="banner-text-wrap">
      <div class="banner-text">
        <div class="highlight">
          <?php if($faculty !== ''): ?>
            <span><?php print $faculty; ?></span>
          <?php endif; ?>
          <h2><?php print render($title); ?> </h2>
          <?php print render($content['body']); ?>
          <span class="banner-feature-type"><?php print render($content['field_faculty_type_stories']); ?></span>
        </div>
      </div>
    </div>
     </a>
   <div class="banner-controls">
     <button class="tabs-previous">&lt;</button>
     <button class="tabs-next">&gt;</button>
    </div>

</div>
