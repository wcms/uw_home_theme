<?php

/**
 * @file
 * Default theme implementation to display a region.
 *
 * Available variables:
 * - $content: The content for this region, typically blocks.
 * - $classes: String of classes that can be used to
 *   style contextually through CSS.
 *   It can be manipulated through the variable
 *   $classes_array from preprocess functions.
 *   The default values can be one or more of the following:
 *   - region: The current template type, i.e., "theming hook".
 *   - region-[name]: The name of the region with
 *     underscores replaced with dashes.
 *   For example, the page_top region would have a region-page-top class.
 *   - $region: The name of the region
 *   variable as defined in the theme's .info file.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 *
 * @see template_preprocess()
 * @see template_preprocess_region()
 * @see template_process()
 */

$uw_theme_branding = variable_get('uw_theme_branding', 'full');
?>
<div id="uw-header" class="uw-header">
    <div id="uw-header-content" class="uw-header--content">
        <?php if (in_array($uw_theme_branding, ['full', 'generic', 'generic_wordmark'])) {?>
            <div id="uw-header-logo" class="uw-header--logo">
                <a id="uw-logo" class="uw-logo" href="//uwaterloo.ca/">University of Waterloo</a>
            </div>
        <?php } ?>
        <?php if ($uw_theme_branding === "full") {?>
            <nav class="nav-university uw-header--nav__university" aria-label="University">
                <ul class="global-menu">
                    <li><a href="https://uwaterloo.ca/admissions/">Admissions</a></li>
                    <li><a href="https://uwaterloo.ca/about/">About Waterloo</a></li>
                    <li><a href="https://uwaterloo.ca/faculties-academics/">Faculties &amp; academics</a></li>
                    <li><a href="https://uwaterloo.ca/offices-services/">Offices &amp; services</a></li>
                    <li><a href="https://uwaterloo.ca/support/">Support Waterloo</a></li>
                    <li><a href="https://uwaterloo.ca/coronavirus/">COVID-19</a></li>
                </ul>
            </nav>
        <?php } ?>
        <div id="uw-header-buttons" class="uw-header--buttons <?php print ($uw_theme_branding !== 'full') ? 'generic_header' : 'non_generic_header'; ?>">
            <div id="uw-header-search-button" class="uw-header--buttons__search" role="search">
              <a href="<?php echo url('search_modal/nojs'); ?>" class="ctools-use-modal search-button" aria-expanded="false"><span class="ifdsu fdsu-search-2"></span><?php print t('Search'); ?></a>
            </div>
          <?php print $content; ?>
        </div>
    </div>
</div>
