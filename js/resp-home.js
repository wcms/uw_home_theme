/**
 * @file
 */

(function ($) {
  "use strict";
  Drupal.behaviors.fdsuTheme = {
    attach: function (context, settings) {
      // Prevent scrolling on Body when menu is open.
      $('button.rmc-nav__navigation-button').once('uw_home_theme-toggle-no-scroll').on('click', function () {
        $('html').toggleClass('no-scroll');
      });
    }
  };
}(jQuery));
(function menuCheck($, window, document) {
    'use strict';
    $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
        // apply timeout to the to event firing
        // so it fires at end of event.
        function debouncer( func ) {
            var timeoutID ,
                timeout =  200;
            return function () {
                var scope = this ,
                    args = arguments;
                clearTimeout( timeoutID );
                timeoutID = setTimeout( function () {
                    func.apply( scope , Array.prototype.slice.call( args ) );
                } , timeout );
            };
        }
        // Check the width of the screen and
        // force the button click if wider that 767px.
        function menuCheckWidth() {
            // Set screenWidth var
            var screenWidth = $(window).width();
            if (screenWidth > 767 ) {
                if($('html').hasClass('no-scroll')){
                    $('button.rmc-nav__navigation-button').click();
                }
            }
        }
        // Listen to event resize and apply the debouncer
        // to the menuCheckWidth function.
        $(window).resize(
            debouncer( function () {
                    menuCheckWidth();
                }
            )
        );
    }
})(window.jQuery, window, window.document);

// Window height.
(function windowHeightAssign($, window) {
    'use strict';
    $(window).load(
        function setWindowHeight() {
            var $window = $(window);
            var $footerHeight = 220;
            var $windowHeight = $(window).innerHeight() - $footerHeight;
            var $headerHeight = 160;
            // Class for setting min-height to main wrap element.
            var $setHeight = 'uw-site--inner';
            $('.' + $setHeight).css('min-height', $windowHeight);
        });
})(window.jQuery, window);
(function checkScrolling($, window, document) {
    'use strict';
    var  $window = $(window);
    var  $windowSize = $(window).width();
    var resizeListener;
    var pause = 0;
    var addEdge;

    $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
        $('#quicktabs-homepage_audience_panel_items').append('<div class="audience-right"></div>');
            if ($(window).width() <= 984) {
                    $('#quicktabs-homepage_audience_panel_items').removeClass('hide-edge');
                }
                else if ($(window).width() > 984) {
                    $('#quicktabs-homepage_audience_panel_items').addClass('hide-edge');
                }
            }

        $(window).resize(function () {
              clearTimeout(resizeListener);
              resizeListener = setTimeout(function () {
                if ($(window).width() <= 984) {
                    var  $windowSize = $(window).width();
                    $('#quicktabs-homepage_audience_panel_items .item-list').css('width' , $(window).width() - 25.6 + 'px');
                    if ($('#quicktabs-homepage_audience_panel_items').hasClass('hide-edge')) {
                        $('#quicktabs-homepage_audience_panel_items').removeClass('hide-edge');
                    }

                }
                else if ($(window).width() > 984) {
                    $('#quicktabs-homepage_audience_panel_items .item-list').css('width' , '984px');
                    $('#quicktabs-homepage_audience_panel_items').addClass('hide-edge');
                }
              },pause);
            });
})(window.jQuery, window, window.document);
(function newsCta($, window, document) {
    'use strict';
    $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
      var newsAppend = $('.news-hide');
      var newsAppendTo = $('.view-homepage-news .view-content ul');
           newsAppend.appendTo(newsAppendTo).wrap('<li/>');
    }
})(window.jQuery, window, window.document);
(function eventsCta($, window, document) {
    'use strict';
    $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
      var eventsAppend = $('.events-hide');
      var eventsAppendTo = $('.view-homepage-events .view-content ul');
           eventsAppend.appendTo(eventsAppendTo).wrap('<li/>');
    }
})(window.jQuery, window, window.document);
(function doubleRainbows($, window, document) {
    'use strict';
    $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
        $("h2").has("a").css("border-bottom", "0px solid transparent");
    }
})(window.jQuery, window, window.document);

// Check if ie.
(function ieCheck($, window) {
    'use strict';
    $(window).load(function isIe() {
        var ua = window.navigator.userAgent;
        var oldIe = ua.indexOf('MSIE ');
        var newIe = ua.indexOf('Trident/');
        if ((oldIe > -1) || (newIe > -1)) {
            return true;
        }
    });
})(window.jQuery, window);

// Is touch device.
(function touchCheck($, window) {
    'use strict';
    $(window).load(function is_touch_device() {
        return 'ontouchstart' in window || 'onmsgesturechange' in window;
    });
})(window.jQuery, window);

// Set up share link.
(function shareLinks($, window, document) {
  'use strict';
  $(document).ready(handleDocumentReady);
  function handleDocumentReady() {
    var $social_links = $('#content .block .share-buttons');
    $('#footer .uw-section-share').append('<div id="block-uw-social-media-sharing-social-media-block" class="block block-uw-social-media-sharing contextual-links-region block-odd"></div>');
    $('#footer .uw-section-share #block-uw-social-media-sharing-social-media-block').append($social_links);
  }
})(window.jQuery, window, window.document);

// Add in daily bulletin and weather block tags.
(function addDBandWeather($, window, document) {
  'use strict';
  $(document).ready(handleDocumentReady);
  function handleDocumentReady() {
    if ($('#daily-bulletin').length > 0) {
      $('#daily-bulletin').parent().addClass('daily-bulletin-block');
    }
    if ($('.weather-box').length > 0) {
      $('.weather-box').parent().addClass('weather-block');
    }
  }
})(window.jQuery, window, window.document);

// Add controls for homepage feature stories, previous/next buttons.
(function ($) {
  Drupal.behaviors.addBannerControls = {
    attach: function (context, setttings) {
    'use strict';
      // RT#558892: Ensure that there is a homepage feature story before applying any work to it.
      if ($('div[id*="quicktabs-container-homepage_feature_stories"]').length > 0) {
        // Get the class list for the div containing the play/pause button.
        var classList = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide) .banner-item').attr("class").split(' ');

        // If there is at least one class, add the faculty class or remove it.
        if (classList.length > 1) {

          // If there is a faculty, add that class to the div with the play/pause button.
          if (classList[3] !== undefined) {
            $('div[id*="quicktabs-homepage_feature_stories"] .item-list').addClass(classList[3]);
          }
          // If there is not a faculty, then remove any faculty classes.
          else {

            // Find any classes that start with org (the colour class), remove it.
            $('div[id*="quicktabs-homepage_feature_stories"]  .item-list').attr('class',
              function (i, c) {
                return c.replace(/(^|\s)org\S+/g, '');
            });
            $('div[id*="quicktabs-homepage_feature_stories"] .item-list').addClass('org_default_bg');
          }
        }
      }

      $(".tabs-next:not(.tabs-next .inactive)").once('homepage_theme').click(function () {
        Drupal.behaviors.addBannerControls.nextSlide();
      });
      Drupal.behaviors.addBannerControls.nextSlide = function(context) {

        // Varible to track whether we can do a change.
        var $change_banner = false;

        // Set the variables that we are going to use, to perform pervious and next functions.
        var $current_banner;
        var $next_banner;
        var $prev_banner;
        var $current_li;
        var $next_li;
        var $prev_li;

        // If there is a next tab, then perform next function.
        if ($('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)').next(".quicktabs-tabpage").length > 0) {

          // Set variables for current banner, next banner, current list item and next list item, using next().
          $current_banner = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)');
          $next_banner = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)').next(".quicktabs-tabpage");
          $current_li = $('div[id*="quicktabs-homepage_feature_stories"] .quicktabs-tabs li.active');
          $next_li = $('div[id*="quicktabs-homepage_feature_stories"] .quicktabs-tabs li.active').next();

          // Set the change banner to true, so that we will switch to the next banner.
          $change_banner = true;
        }
        // If there is no next homepage feature banner, we have to use the first (i.e. wrap around).
        else {

          // Ensure that there is a next homepage feature banner.
          if ($('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)').first().length > 0) {

            // Set the variable for current banner, next banner, current list item and next list item.
            $current_banner = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)');
            $next_banner = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage').first();
            $current_li = $('div[id*="quicktabs-homepage_feature_stories"] .quicktabs-tabs li.active');
            $next_li = $('div[id*="quicktabs-homepage_feature_stories"] .quicktabs-tabs li').first();

            // Set the change banner to true, so that we will switch to the next banner.
            $change_banner = true;
          }
        }

        // If we are going to perform the change to the next homepage feautre banner.
        if ($change_banner) {

          // Set the current homepage feautre banner to hide.
          $current_banner.addClass('quicktabs-hide');

          // Remove the hide from the next homepage feature banner (showing the next banner).
          $next_banner.removeClass('quicktabs-hide');

          // Remove all the styling for the next banner (sometimes there was a display none on it).
          $next_banner.removeAttr("style");

          // Remove the active class from the current list item.
          $current_li.removeClass('active');

          // Adding the active class to the next list item.
          $next_li.addClass('active');

          // Find any classes that start with org (the colour class), remove it.
          $('div[id*="quicktabs-homepage_feature_stories"] .item-list').attr('class',
            function (i, c) {
              return c.replace(/(^|\s)org\S+/g, '');
          });

          // Get the class list for the div containing the play/pause button.
          var classList = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide) .banner-item').attr("class").split(' ');

          // If there is at least one class, add the faculty class or remove it.
          if (classList.length > 1) {

            // If there is a faculty, add that class to the div with the play/pause button.
            if (classList[3] !== undefined) {
              $('div[id*="quicktabs-homepage_feature_stories"] .item-list').addClass(classList[3]);
            }
            else {
              $('div[id*="quicktabs-homepage_feature_stories"] .item-list').addClass('org_default_bg');
            }
          }
        }
      };

      // If there are tabs, add in click functions for previous button.
      $(".tabs-previous").once("homepage_theme").click(function () {
        Drupal.behaviors.addBannerControls.prevSlide();
      });
      Drupal.behaviors.addBannerControls.prevSlide = function(context) {

        // Varible to track whether we can do a change.
        var $change_banner = false;

        // Set the variables that we are going to use, to perform pervious and next functions.
        var $current_banner;
        var $next_banner;
        var $prev_banner;
        var $current_li;
        var $next_li;
        var $prev_li;

        // If there is a next tab, then perform previous function.
        if ($('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)').prev(".quicktabs-tabpage").length > 0) {

          // Set variables for current banner, prev banner, current list item and prev list item, using prev().
          $current_banner = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)');
          $prev_banner = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)').prev(".quicktabs-tabpage");
          $current_li = $('div[id*="quicktabs-homepage_feature_stories"] .quicktabs-tabs li.active');
          $prev_li = $('div[id*="quicktabs-homepage_feature_stories"] .quicktabs-tabs li.active').prev();

          // Set the change banner to true, so that we will switch to the next banner.
          $change_banner = true;
        }
        // If there is no next homepage feature banner, we have to use the last (i.e. wrap around).
        else {

          // Ensure that there is a previous homepage feature banner.
          if ($('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)').last().length > 0) {

            // Set the variable for current banner, prev banner, current list item and prev list item, using last().
            $current_banner = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide)');
            $prev_banner = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage[id*="homepage_feature_stories"]').last();
            $current_li = $('div[id*="quicktabs-homepage_feature_stories"] .quicktabs-tabs li.active');
            $prev_li = $('div[id*="quicktabs-homepage_feature_stories"] .quicktabs-tabs li').last();

            // Set the change banner to true, so that we will switch to the prev banner.
            $change_banner = true;
          }
        }

        // If we are going to perform the change to the previous homepage feautre banner.
        if ($change_banner) {

          // Set the current homepage feautre banner to hide.
          $current_banner.addClass('quicktabs-hide');

          // Remove the hide from the prev homepage feature banner (showing the prev banner).
          $prev_banner.removeClass('quicktabs-hide');

          // Remove all the styling for the prev banner (sometimes there was a display none on it).
          $prev_banner.removeAttr("style");

          // Remove the active class from the current list item.
          $current_li.removeClass('active');

          // Adding the active class to the prev list item.
          $prev_li.addClass('active');

          // Find any classes that start with org (the colour class), remove it.
          $('div[id*="quicktabs-homepage_feature_stories"] .item-list').attr('class',
            function (i, c) {
              return c.replace(/(^|\s)org\S+/g, '');
          });

          // Get the class list for the div containing the play/pause button.
          var classList = $('div[id*="quicktabs-container-homepage_feature_stories"] .quicktabs-tabpage:not(.quicktabs-hide) .banner-item').attr("class").split(' ');

          // If there is at least one class, add the faculty class or remove it.
          if (classList.length > 1) {

            // If there is a faculty, add that class to the div with the play/pause button.
            if (classList[3] !== undefined) {
              $('div[id*="quicktabs-homepage_feature_stories"] .item-list').addClass(classList[3]);
            }
            else {
              $('div[id*="quicktabs-homepage_feature_stories"] .item-list').addClass('org_default_bg');
            }
          }
        }
      };
    }
  };
})(jQuery);

// Add class if ie10,ie11.
(function addIeClass($, window, document) {
    'use strict';
    $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
        // CONDITIONAL COMMENTS NO LONGER WORK IN IE10 need to get js or php to identify bropwser's user agent.
        if ($.browser.msie && $.browser.version == '10.0') {
            $("html").addClass("ie10");
        }
        if ($.browser.msie && $.browser.version == '11.0') {
            $("html").addClass("ie11");
        }
    }
})(window.jQuery, window, window.document);

// Checks if an image has a formatting (iamge-left, image-right) and if it is 50% of the window with
// will remove the classes and add the image-center which will center and place text underneath.
(function checkFormattingWithImages($, window, document) {
  'use strict';

  // On each window, need to check on load and on resize for images.
  $(window).on({
    // On window load.
    load:function () {
      // If the window width is less than 480, check image size and set appropriate classes if large images.
      if ($(window).width() < 480) {
        // Step through each image.
        $('.content img').each(function (index) {
          // If there are indents (image-left and image-right), check if image larger then 50%.
          if ($(this).hasClass('image-left') || $(this).hasClass('image-right')) {
            // If image is larger than 50%, add on extra class (image-full) to center and place text below.
            if ($(this).width() > ($(window).width() / 2)) {
              // If there is no class image-full add it to the image.
              if (!$(this).hasClass('image-full')) {
                $(this).addClass('image-full');
              }
            }
            // If the image is not larger than 50%, remove the class image-full.
            else {
              // If there is the class, remove it.
              if ($(this).hasClass('image-full')) {
                $(this).removeClass('image-full');
               }
            }
          }
        });

        // If there is a sidebar and news/events, move the sidebar below news/events.
        if ($('.content').find('.uw-site-homepage-tabs')) {
          $("#block-uw-ct-event-front-page").append($('#site-sidebar-wrapper'));
        }

      }
    },
    // On resize.
    resize:function () {
      // If the window width is less than 480, check image size and set appropriate classes if large images.
      if ($(window).width() < 480) {
        // Step through each image.
        $('.content img').each(function (index) {
          // If there are indents (image-left and image-right), check if image larger then 50%.
          if ($(this).hasClass('image-left') || $(this).hasClass('image-right')) {
            // If image is larger than 50%, add on extra class (image-full) to center and place text below.
            if ($(this).width() > ($(window).width() / 2)) {
              // If there is no class image-full add it to the image.
              if (!$(this).hasClass('image-full')) {
                $(this).addClass('image-full');
              }
            }
          }
        });

        // If there is a sidebar and news/events, move the sidebar below news/events.
        if ($('.content').find('.uw-site-homepage-tabs')) {
          $("#block-uw-ct-event-front-page").append($('#site-sidebar-wrapper'));
        }

      }
      // If the window is larger than 480, remove the class image-full.
      else {
        // Step through each image.
         $('.content img').each(function (index) {
          // If there are indents (image-left and image-right), check if image-full class is enabled.
          if ($(this).hasClass('image-left') || $(this).hasClass('image-right')) {
            // If the image-full class is present, remove it.
            if ($(this).hasClass('image-full')) {
              $(this).removeClass('image-full');
            }
          }
        });
      }
    }
  });
})(window.jQuery, window, window.document);
// Top button.
(function topButton($, window, document) {
    'use strict';
    $(document).ready(scrollToTop);
    function scrollToTop() {
        $('.uw-top-button').click(function () {
            $("html, body").animate({ scrollTop: 0 }, 300);
            return false;
        });
    }
})(window.jQuery, window, window.document);

// Set up share link.
(function shareLinks($, window, document) {
    'use strict';
    $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
        var $socialLinks = $('#block-uw-social-media-sharing-social-media-block');
        if ($socialLinks.length) {
            $socialLinks.prependTo($('.uw-section-share'));
        }
        $('.uw-footer-social-button').on('click', function () {
            $('.uw-section-share').slideToggle('fast');
            return false;
        });
    }
})(window.jQuery, window, window.document);
(function menuClick($, window, document) {
    'use strict';
    $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
        var $menu_toggle = $('#site');
        var $menu_tab = $('.responsive-nav-menu .tabs .content');
        var $menu_button = $('.navigation-button__toggle');
        var setTabHeight = function () {
            var $window = $(window);
            var $tab_height = $('.uw-site--inner').innerHeight();
            var $set_tab_height = '.responsive-nav-menu .tabs';
            $($set_tab_height).css('min-height',$tab_height);
        };
        var resetTabHeight = function () {
            var $set_tab_height = '.responsive-nav-menu .tabs';
            $($set_tab_height).css('min-height', '');
        };
        var openMenu = function () {
            $menu_toggle.attr({
                'data-nav-visible': 'true'
            });
            $menu_button.attr({
                'aria-expanded': 'true'
            });
            setTabHeight();
        };
        var closeMenu = function () {
            $menu_toggle.attr({
                'data-nav-visible': 'false'
            });
            $menu_button.attr({
                'aria-expanded': 'false'
            });
            resetTabHeight();
        };
        $menu_button.on('click', function (e) {
          if ($(this).attr('aria-expanded') == 'true') {
            closeMenu();
          }
          else {
            openMenu();
          }
        });
    }
})(window.jQuery, window, window.document);
