/**
 * @file
 */

/**
 *
 */

function counterObject(element, countTo) {

    var _this = this;
    this.digits = countTo.toString().length;
    this.countTo = countTo;
    this.element = element;

    this.changeCounter = function (number) {
        if (number <= this.countTo) {
                this.element.text(this.renderNumber(Math.round(number)));
                setTimeout(function () {_this.changeCounter(number + (_this.countTo / 42));}, 42);
        }
            else {
                this.element.text(this.renderNumber(this.countTo));
        }
    }

    this.renderNumber = function (number) {
        nStr = number.toString();
        while (nStr.length < this.digits) {
            nStr = '0' + nStr;
        }
        return this.addCommas(nStr);
    }

    this.addCommas = function (nStr) {

        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

}

(function ($) {
    $.fn.countMeIn = function () {
        this.each(function () {
            $this = $(this);
            $this.width($this.width());
            var number = parseFloat($this.text().replace(/\D/g, ''));
            if (!isNaN(number)) {
                new counterObject($this, number).changeCounter(0);
            }
        });
  };

  $(function () {
    $('.countin', '#count-container').countMeIn();
  });

})(jQuery);
